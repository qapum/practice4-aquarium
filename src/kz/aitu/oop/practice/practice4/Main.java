package kz.aitu.oop.practice.practice4;

import kz.aitu.oop.practice.practice4.models.Accessories;
import kz.aitu.oop.practice.practice4.models.Fish;
import kz.aitu.oop.practice.practice4.models.Reptile;
import kz.aitu.oop.practice.practice4.repositories.AquariumAccessoriesFileRepository;
import kz.aitu.oop.practice.practice4.repositories.AquariumEntitiesFileRepository;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        AquariumEntitiesFileRepository aquariumEntitiesFileRepository = new AquariumEntitiesFileRepository("D:\\galamshar\\IntelijProject\\practice4\\src\\kz\\aitu\\oop\\practice\\practice4\\entities.txt");
        AquariumAccessoriesFileRepository aquariumAccessoriesFileRepository = new AquariumAccessoriesFileRepository("D:\\galamshar\\IntelijProject\\practice4\\src\\kz\\aitu\\oop\\practice\\practice4\\accessories.txt");
        Aquarium aquarium = new Aquarium(aquariumAccessoriesFileRepository, aquariumEntitiesFileRepository);
        aquarium.AddEntity(new Fish(1,"Sazan",3.49));
        aquarium.AddEntity(new Reptile(2,"Reptile",5.79));

        System.out.println(aquarium.GetTotalPrice());
    }
}
