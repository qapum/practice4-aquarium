package kz.aitu.oop.practice.practice4.models;

public enum Accessories {
    SAND(32.00),FILTER(45.00),STONEDECOR(9.99);

    private double price;
    Accessories(double price){
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Accessories | " + name() + " | " + getPrice();
    }
}
