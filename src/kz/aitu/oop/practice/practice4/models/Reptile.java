package kz.aitu.oop.practice.practice4.models;

public class Reptile extends AquariumEntity {
    private int id;
    private String name;
    private double price;

    public Reptile() {
    }

    public Reptile(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Reptile | " + getId() + " | " + getName() + " | " + getPrice();
    }
}
