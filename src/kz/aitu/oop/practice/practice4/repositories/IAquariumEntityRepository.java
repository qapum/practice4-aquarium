package kz.aitu.oop.practice.practice4.repositories;

import kz.aitu.oop.practice.practice4.models.AquariumEntity;

import java.io.IOException;
import java.util.ArrayList;

public interface IAquariumEntityRepository {
    void Add(AquariumEntity aquariumEntity) throws IOException;
    void RemoveByID(int id);
    void Save() throws IOException;
    ArrayList<AquariumEntity> GetList();
}
