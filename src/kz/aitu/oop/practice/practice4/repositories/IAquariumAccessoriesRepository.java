package kz.aitu.oop.practice.practice4.repositories;

import kz.aitu.oop.practice.practice4.models.Accessories;
import kz.aitu.oop.practice.practice4.models.AquariumEntity;

import java.io.IOException;
import java.util.ArrayList;

public interface IAquariumAccessoriesRepository {
    void Add(Accessories accessory) throws IOException;
    void RemoveByName(String name);
    void Save() throws IOException;
    ArrayList<Accessories> GetList();
}
