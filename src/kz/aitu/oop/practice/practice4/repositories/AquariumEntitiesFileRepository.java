package kz.aitu.oop.practice.practice4.repositories;

import kz.aitu.oop.practice.practice4.models.AquariumEntity;
import kz.aitu.oop.practice.practice4.models.Fish;
import kz.aitu.oop.practice.practice4.models.Reptile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Scanner;

public class AquariumEntitiesFileRepository implements IAquariumEntityRepository {
    private final ArrayList<AquariumEntity> aquariumEntities = new ArrayList<>();
    private File file;
    private Scanner fileScanner;

    public AquariumEntitiesFileRepository() throws FileNotFoundException {
        file = new File("file.txt");
        fileScanner = new Scanner(file);
    }

    public AquariumEntitiesFileRepository(String fileName) throws FileNotFoundException {
        this.file = new File(fileName);
        fileScanner = new Scanner(file);
    }

    public AquariumEntitiesFileRepository(File file) throws FileNotFoundException {
        this.file = file;
        fileScanner = new Scanner(file);
    }

    public void ReadAll() {
        fileScanner.useDelimiter("|");
        while (fileScanner.hasNext()){
            String className = fileScanner.next();
            if (className == "Fish"){
                int id = fileScanner.nextInt();
                String name = fileScanner.next();
                double price = fileScanner.nextDouble();
                Add(new Fish(id,name,price));
            } else if (className == "Reptile"){
                int id = fileScanner.nextInt();
                String name = fileScanner.next();
                double price = fileScanner.nextDouble();
                Add(new Reptile(id,name,price));
            }
        }
    }
    @Override
    public void Add(AquariumEntity aquariumEntity) {
        aquariumEntities.add(aquariumEntity);
    }

    @Override
    public void RemoveByID(int id) {
        aquariumEntities.removeIf(entity -> entity.getId() == id);
    }

    @Override
    public void Save() throws IOException {
        String content = "";
        aquariumEntities.sort(AquariumEntity::compareTo);
        for (AquariumEntity entity: aquariumEntities) {
            content += entity + "\n";
        }
        Files.write(file.toPath(),content.getBytes());
    }

    @Override
    public ArrayList<AquariumEntity> GetList() {
        return aquariumEntities;
    }

}
