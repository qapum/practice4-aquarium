package kz.aitu.oop.practice.practice4.repositories;

import kz.aitu.oop.practice.practice4.models.Accessories;
import kz.aitu.oop.practice.practice4.models.AquariumEntity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Scanner;

public class AquariumAccessoriesFileRepository implements IAquariumAccessoriesRepository {
    private ArrayList<Accessories> accessories;
    private File file;
    private Scanner fileScanner;
    public AquariumAccessoriesFileRepository() throws FileNotFoundException {
        accessories = new ArrayList<Accessories>();
        file = new File("file.txt");
        fileScanner = new Scanner(file);
    }
    public AquariumAccessoriesFileRepository(String fileName) throws FileNotFoundException {
        this.file = new File(fileName);
        fileScanner = new Scanner(file);
    }

    public AquariumAccessoriesFileRepository(File file) throws FileNotFoundException {
        this.file = file;
        fileScanner = new Scanner(file);
    }

    public AquariumAccessoriesFileRepository(ArrayList<Accessories> accessories) throws FileNotFoundException {
        this.accessories = accessories;
    }

    @Override
    public void Add(Accessories accessory) throws IOException {
        accessories.add(accessory);
    }

    @Override
    public void RemoveByName(String name) {
        accessories.removeIf(accessory -> accessory.name() == name);
    }

    @Override
    public void Save() throws IOException {
        String content = "";
        for (Accessories accessory: accessories) {
            content += accessory + "\n";
        }
        Files.write(file.toPath(),content.getBytes());
    }

    @Override
    public ArrayList<Accessories> GetList() {
        return accessories;
    }


}
