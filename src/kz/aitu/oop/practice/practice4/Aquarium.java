package kz.aitu.oop.practice.practice4;

import kz.aitu.oop.practice.practice4.models.Accessories;
import kz.aitu.oop.practice.practice4.models.AquariumEntity;
import kz.aitu.oop.practice.practice4.repositories.IAquariumAccessoriesRepository;
import kz.aitu.oop.practice.practice4.repositories.IAquariumEntityRepository;

import java.io.IOException;

public class Aquarium {
    private IAquariumAccessoriesRepository aquariumAccessoriesRepository;
    private IAquariumEntityRepository aquariumEntityRepository;

    public Aquarium() {
    }

    public Aquarium(IAquariumAccessoriesRepository aquariumAccessoriesRepository, IAquariumEntityRepository aquariumEntityRepository) {
        this.aquariumAccessoriesRepository = aquariumAccessoriesRepository;
        this.aquariumEntityRepository = aquariumEntityRepository;
    }

    void AddEntity(AquariumEntity aquariumEntity) throws IOException {
        aquariumEntityRepository.Add(aquariumEntity);
    }
    void AddAccessory(Accessories accessory) throws IOException {
        aquariumAccessoriesRepository.Add(accessory);
    }

    void RemoveEntityByID(int id){
        aquariumEntityRepository.RemoveByID(id);
    }

    void RemoveAccessoryByName(String name){
        aquariumAccessoriesRepository.RemoveByName(name);
    }
    void SaveAquarium() throws IOException {
        aquariumEntityRepository.Save();
        aquariumAccessoriesRepository.Save();
    }
    double GetTotalPrice(){
        double totalPrice = 0;
        for (AquariumEntity aquariumEntity: aquariumEntityRepository.GetList()) {
            totalPrice += aquariumEntity.getPrice();
        }
        for (Accessories accessories : aquariumAccessoriesRepository.GetList()){
            totalPrice += accessories.getPrice();
        }
        return totalPrice;
    }
}
